# NRoach44 Unbiquiti Unifi Security Gateway 3P Buildroot External tree

This will build a complete, flashable Linux image for the Ubiquit Unifi Security Gateway.

The config here has been designed to mirror the official partition layout and be directly flashable to the official USB stick. If you wish to change the partition layout, look in `board/ubiquitiusg3p/genimage.cfg`. That's a genimage file, and all offsets / sizes are in bytes.

## Usage

Have a buildroot release extracted / synced locally. No special requirements other than the normal BR ones.

```
export BR2_EXTERNAL=$(pwd)
mkdir build
make O=$(pwd)/build -C <path to buildroot tree> ubiquitiusg3p_defconfig
cd build
```

Or, from your choice of build directory:

```
BR2_EXTERNAL="$HOME/media/code/nr44/br-external-common/:$HOME/media/code/nr44/br-usg3p/" make -C "$HOME/media/code/buildroot/buildroot-git" O=$(pwd) ubiquitiusg3p_defconfig
```

Then `make xconfig`, `make linux-xconfig` to configure, then `make` to build a disk image.

To apply the disk image:

```
sudo dd if=images/sdimage.img status=progress conv=fdatasync,sparse bs=1M of=<TARGET DISK>
```
(`images/sdimage,img` is a sparse image, so using this command will only write the "active" data, reducing time and writes)

## Misc

### LEDs
```
cat /sys/devices/platform/leds/leds/ubnt\:white\:dome/trigger
```

### u-boot

Original envs:
```
Octeon ubnt_e120# printenv
bootcmd=fatload usb 0 $loadaddr vmlinux.64;bootoctlinux $loadaddr coremask=0x3 root=/dev/sda2 rootdelay=15 rw rootsqimg=squashfs.img rootsqwdir=w mtdparts=phys_mapped_flash:512k(boot0),512k(boot1),64k@1024k(eeprom)
bootdelay=0
baudrate=115200
download_baudrate=115200
nuke_env=protect off $(env_addr) +$(env_size);erase $(env_addr) +$(env_size)
autoload=n
ethact=octeth0
loadaddr=0x9f00000
numcores=2
stdin=serial
stdout=serial
stderr=serial
env_addr=0x1fbfe000
env_size=0x2000
flash_base_addr=0x1f400000
flash_size=0x800000
uboot_flash_addr=0x1f480000
uboot_flash_size=0x70000
flash_unused_addr=0x1f4f0000
flash_unused_size=0x710000
bootloader_flash_update=bootloaderupdate

Environment size: 675/8188 bytes
```
