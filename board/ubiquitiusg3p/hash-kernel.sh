#!/bin/bash

echo [hash-kernel] Generating checksum of kernel

md5sum "$BINARIES_DIR"/vmlinux | cut -c -32 > "$BINARIES_DIR"/vmlinux.64.md5
